@extends('blog.master')

@section('content')

<div class="ml-3 mt-2 mr-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Create New</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/comments" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="komentar">komentar</label>
                    <input type="text" class="form-control" id="komentar" value="komentar" name=" komentar" placeholder="Enter Title">
                    @error('komentar')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="posts_id">komentar</label>
                    <input type="text" class="form-control" id="posts_id" value="" name="posts_id" placeholder="Enter Title">
                    @error('posts_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
    </div>
</div>

@endsection