@extends('blog.master')

@section('content')
<div><a href="/posts/create" class="btn btn-info btn-sm mb-2 ml-4 mt-2">Tambah</a></div>
@if(session('success'))
<div class="alert alert-success">
  {{ session('success') }}
</div>
@endif
@foreach($posts as $posts2)


<div class="col-md-12">
  <div class="card ml-2 mr-2 mt-2">
    <h4><a href="/profiles/{{Auth::user()->id}}" class="ml-2">{{$posts2->user->profile->nama_lengkap }}</a></h4>
    <div class="col-md-4 ml-2" id="image">
      <h1 class=" ml-2">{{ $posts2->judul }}</h1><br>
      <img src="{{asset('images/'.$posts2->gambar)}}">

      <p class="card-text ml-2">{{$posts2->isi}}</p>
      <div class="row ml-2">
        @guest
        <a href="/posts/{{$posts2->id}}" class="btn btn-info btn-sm mb-2 mt-2">Detail</a>

        @else
        <a href="/posts/{{$posts2->id}}" class="btn btn-info btn-sm mb-2 mt-2">Detail</a>
        <a href="/posts/{{$posts2->id}}/edit" class="btn btn-warning btn-sm mb-2 mt-2 ml-2">Edit</a>
        <form action="/posts/{{$posts2->id}}" method="post">
          @csrf
          @method('DELETE')
          <input type="submit" value="Delete" class="btn btn-danger btn-sm ml-2 mb-2 mt-2">
        </form>
      </div>
    </div>
  </div>
</div>
@endguest



@endforeach

@endsection