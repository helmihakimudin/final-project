@extends('blog.master')

@section('content')

<img src="{{asset('images/'.$posts2->gambar)}}" class="card-img-top" style="width: auto; height:auto;" alt="...">
<div class="card-body">
  <h3>
    <div class="user">{{$posts2->user->profile->nama_lengkap }}</div>
  </h3>
  <p>
    <h5 class="card-title">{{$posts2->judul}}</h5>
    <p class="card-text">{{$posts2->isi}}</p>
    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
</div>
</div>

@endsection