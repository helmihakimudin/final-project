@extends('blog.master')

@section('content')

<div class="ml-3 mt-2 mr-3">
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Buat Baru</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/posts" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Judul</label>
          <input type="text" class="form-control" id="judul" value="{{old('judul','')}}" name="judul" placeholder="Enter Title">
          @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>

        <div class="form-group">
          <label for="isi">Isi</label>
          <textarea class="form-control" id="isi" value="{{old('isi','')}}" name="isi" placeholder="Enter Body" style="height: 180px;"></textarea>
          @error('isi')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror

        </div>

        <!--    <div class="form-group">
                    <label for="users_id">Body</label>
                    <input type="text" class="form-control" id="users_id" value="{{old('users_id','')}}" name="users_id" placeholder="Enter User">
                    @error('users_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                  </div> -->

        <div class="form-group">
          <label for="gambar">Gambar</label>
          <input type="file" class="form-control" id="gambar" value="" name="gambar">
          @error('gambar')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror

        </div>

      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
</div>

@endsection