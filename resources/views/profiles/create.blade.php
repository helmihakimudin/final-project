@extends('layouts.app')

@section('content')

<div class="ml-3 mt-2 mr-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Data Profile</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/profiles" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama_lengkap">Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama_lengkap" value="{{old('nama_lengkap','')}}" name="nama_lengkap" placeholder="Enter Title">
                    @error('nama_lengkap')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="no_telpon">Nomor Telepon</label>
                    <input type="text" class="form-control" id="no_telpon" value="{{old('no_telpon','')}}" name="no_telpon" placeholder="Enter Title">
                    @error('no_telpon')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>


                  <!-- div class="form-group">
                    <label for="isi">Body</label>
                    <textarea class="form-control" id="isi" value="{{old('isi','')}}" name="isi" placeholder="Enter Body" style="height: 180px;"></textarea>
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                  </div> -->

               <!--    <div class="form-group">
                    <label for="users_id">Body</label>
                    <input type="text" class="form-control" id="users_id" value="{{old('users_id','')}}" name="users_id" placeholder="Enter User">
                    @error('users_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                  </div> -->

                   <div class="form-group">
                    <label for="photo">Photo</label>
                    <input type="file" class="form-control" id="photo" value="" name="photo">
                    @error('photo')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
           </div>

@endsection