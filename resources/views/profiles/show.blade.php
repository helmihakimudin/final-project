@extends('blog.master')

@section('content')

<img src="{{asset('images/'.$profile->photo)}}" class="card-img-top" style="width: auto; height:auto;" alt="...">
<div class="card-body">
    <p>
        <h5 class="card-title">Nama: {{$profile->nama_lengkap}}</h5>
        <p class="card-text">No Telepon: {{$profile->no_telpon}}</p>
        <a href="/profiles/{{$profile->id}}/edit" class="btn btn-info btn-sm mb-2 mt-2 ml-2">Edit Profile </a>
</div>

@endsection