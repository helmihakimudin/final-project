<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">UrBlog</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="/profiles/{{Auth::user()->id}}" class="d-block">{{ Auth::user()->name }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/posts" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Beranda

            </p>
          </a>
        </li>
        <!--  @php($profiles as $key => $profile2)
          <li class="nav-item">
            <a href="/profiles/{{$profile2->id}}/edit" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Edit Profile
                
              </p>
            </a>
          </li>
          @endphp -->


        <!--           @guest
           <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">
                                        
              <i class="nav-icon fa fa-user"></i>
              <p>
                {{ __('Login') }}
              </p>
            </a>
          </li>
          @if (Route::has('register'))
           <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">
                                        
              <i class="nav-icon fa fa-user"></i>
              <p>
                {{ __('Register') }}
              </p>
            </a>
          </li>
           @endif
            
             @else -->

        <li class="nav-item">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

            <i class="nav-icon fa fa-user"></i>
            <p>
              {{ __('Logout') }}
            </p>
          </a>
        </li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>



        <!-- @endguest -->
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>