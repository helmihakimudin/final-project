<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/home');
});

Route::resource('posts', 'PostController')->middleware('auth');

Auth::routes();
//Route::resource('comments', 'CommentController')->middleware('auth');
//Route::get('/comments/create', 'CommentController@create')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profiles/create', 'ProfileController@create');
Route::post('/profiles', 'ProfileController@store');
// Route::get('/profiles', 'PertanyaanController@index');
Route::get('/profiles/{id}', 'ProfileController@show');
Route::get('/profiles/{id}/edit', 'ProfileController@edit');
Route::put('/profiles/{id}', 'ProfileController@update');
// Route::delete('/profiles/{id}', 'PertanyaanController@destroy');
