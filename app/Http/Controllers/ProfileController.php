<?php

namespace App\Http\Controllers;

use DB;
use App\Profile;
use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    //
    public function create()
    {
        //
        return view('profiles.create');
    }

    public function store(Request $request)
    {
        //
        $request->validate([
            'nama_lengkap' => 'required',
            'no_telpon' => 'required'

        ]);

        $profile = new Profile;

        $profile->nama_lengkap = $request->nama_lengkap;
        $profile->no_telpon = $request->no_telpon;
        $profile->users_id = Auth::id();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/', $filename);
            $profile->photo = $filename;
            # code...
        }

        $profile->save();

        return redirect('/posts')->with('success', 'Data Berhasil Disimpan!');
    }

    public function edit($id)
    {
        //
        $profile = Profile::find($id);
        return view('profiles.edit', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        //
        $profile = Profile::find($id);

        $profile->nama_lengkap = $request->nama_lengkap;
        $profile->no_telpon = $request->no_telpon;
        $profile->users_id = Auth::id();
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/', $filename);
            $profile->photo = $filename;
            # code...
        }

        $profile->save();

        return redirect('/posts')->with('success', 'Berhasil Update!');
    }
    public function show($id)
    {
        $profile = Profile::find($id);
        return view('profiles.show', compact('profile'));
    }
}
