<?php

namespace App\Http\Controllers;

use DB;
use App\Post;
use Illuminate\Http\Request;
use Auth;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    public function index()
    {
        //
        $posts = Post::all();
        return view('posts.index', compact('posts'));
    }


    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'

        ]);

        $post = new Post;

        $post->judul = $request->judul;
        $post->isi = $request->isi;
        // $post->gambar = $request->file('gambar')->store('images');
        $post->users_id = Auth::id();
        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/', $filename);
            $post->gambar = $filename;
            # code...
        }

        $post->save();

        // $post = Post::create([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"],
        //     "gambar" => $request["gambar"],
        //     "users_id" => Auth::id()
        // ]);




        return redirect('/posts')->with('success', 'Post Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $posts2 = Post::find($id);
        return view('posts.show', compact('posts2'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $posts2 = Post::find($id);
        return view('posts.edit', compact('posts2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $query = Post::find($id);

        $query->judul = $request->judul;
        $query->isi = $request->isi;
        // $post->gambar = $request->file('gambar')->store('images');
        $query->users_id = Auth::id();

        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/', $filename);
            $query->gambar = $filename;
            # code...
        }

        $query->save();
        return redirect('/posts')->with('success', 'Berhasil Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Post::destroy($id);
        return redirect('/posts')->with('success', 'Berhasil delete');
    }
}
