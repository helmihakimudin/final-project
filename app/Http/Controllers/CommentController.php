<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Auth;
use App\Post;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comment = Comment::all();
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('comments.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'komentar' => 'required'
        ]);

        $comment = new Comment;

        $comment->komentar = $request->komentar;
        // $post->gambar = $request->file('gambar')->store('images');
        $comment->users_id = Auth::id();
        //$comment->posts_id = $request->posts_id;
        $comment->save();
        //{{$posts2->user->profile->nama_lengkap }}

        // $post = Post::create([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"],
        //     "gambar" => $request["gambar"],
        //     "users_id" => Auth::id()
        // ]);




        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment2 = Post::find($id);
        return view('comments.create', compact('posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment2 = Comment::find($id);
        return view('comments.create', compact('comment2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $query = Comment::find($id);

        $query->komentar = $request->komentar;
        $query->posts_id = $request->posts->posts_id;
        // $post->gambar = $request->file('gambar')->store('images');
        $query->users_id = Auth::id();


        $query->save();
        return redirect('/posts')->with('success', 'Berhasil Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
